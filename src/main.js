/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import VueSweetalert2 from 'vue-sweetalert2';
import ArgonDashboard from './plugins/argon-dashboard'
import axios from 'axios'
import store from './store'

Vue.config.productionTip = false
Vue.use(VueSweetalert2);

Vue.use(ArgonDashboard)
var app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

let http = axios.create({
  baseURL: 'http://localhost:8081/',
  timeout: 5000
});

Vue.prototype.$http = http;

http.interceptors.response.use((response) => {return response}, (error) => {
  if (error.response.status && error.response.status === 401) {
    store.getters.auth.loggedIn = false
    localStorage.removeItem('token')
    localStorage.removeItem('expireToken')
    router.push({name: 'login'})
  }

  return Promise.reject(error)
});
