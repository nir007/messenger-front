import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import LandingLayout from '@/layout/LandingLayout'
import auth from './middlewares/Auth.js'
Vue.use(Router)

const routes = [
  {
    path: '/dashboard',
    redirect: 'dashboard',
    component: DashboardLayout,
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue'),
        meta: {
          middleware: [
            auth
          ]
        }
      },
      {
        path: '/icons',
        name: 'icons',
        component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
      },
      {
        path: '/manager',
        name: 'manager',
        component: () => import(/* webpackChunkName: "demo" */ './views/Manager.vue')
      },
      {
        path: '/applications',
        name: 'applications',
        component: () => import(/* webpackChunkName: "demo" */ './views/Applications.vue')
      },
      {
        path: '/application/:id',
        name: 'application',
        component: () => import(/* webpackChunkName: "demo" */ './views/Application.vue')
      },
      {
        path: '/users/:id',
        name: 'users',
        component: () => import(/* webpackChunkName: "demo" */ './views/Users.vue')
      },
      {
        path: '/user/:id/:appId',
        name: 'user',
        component: () => import(/* webpackChunkName: "demo" */ './views/User.vue')
      },
      {
        path: '/tables',
        name: 'tables',
        component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
      }
    ]
  },
  {
    path: '/login',
    redirect: 'login',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import('./views/landing/Login.vue')
      },
      {
        path: '/register',
        name: 'register',
        component: () => import('./views/landing/Register.vue')
      },
      {
        path: '/serverError',
        name: 'serverError',
        component: () => import('./views/landing/ServerError.vue')
      }
    ]
  },
  {
    path: '/',
    redirect: '/site',
    component: LandingLayout,
    children: [
      {
        path: '/site',
        name: '',
        component: () => import(/* webpackChunkName: "demo" */ './views/landing/Index.vue')
      }
    ]
  }
]

import VueRouter from "vue-router";
import store from "./store";
import middlewarePipeline from './middlewares/middlewarePipeline';

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkExactActiveClass: "active",
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
});

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }
  const middleware = to.meta.middleware
  const context = {
    to,
    from,
    next,
    store
  }
  return middleware[0]({
    ...context,
    next: middlewarePipeline(context, middleware, 1)
  })
})

export default router;
