import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    user: {
      loggedIn: false,
    }
  },
  getters: {
    auth(state) {
      if (localStorage.token && localStorage.token.length > 10) {
        state.user.loggedIn = true;
      }
      return state.user
    }
  }
})